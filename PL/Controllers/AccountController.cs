﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        

        public AccountController(IUserService service, UserManager<User> userManager, SignInManager<User> signInManager, IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userService = service;
            _mapper = mapper;
        }


        // GET: api/<AccountController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<AccountController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/account/login
        [HttpPost("login")]
        public async Task<ActionResult> LogIn([FromBody]LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (!user.isEnabled) return BadRequest(new { response = "You are banned" });
            var identity = await GetIdentity(model.UserName, model.Password);
            if (identity == null)
            {
                return BadRequest(new { response = "Invalid username or password." });
            }
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };
            var u = User.Identity.Name;
            
            return Ok(response);
        }

        // POST api/account/login
        [HttpPost("signup")]
        public async Task<ActionResult> SignUp([FromBody] RegisterModel model)
        {
            if (await _userManager.FindByNameAsync(model.Username) != null) return BadRequest(new { response = "This username already exists" });
            else if (model.ConfirmPassword != model.Password) return BadRequest(new { response = "Passwords do not match"});
            else if (model.Password.Length < 8) return BadRequest(new { response = "Password length must be more than 8 characters" });
            

            if (ModelState.IsValid)
            {
                var user = new User { UserName = model.Username, isEnabled = true };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "user");
                    //await _signInManager.SignInAsync(user, false);

                }
                else
                {
                    return BadRequest();
                }

                return Ok();
            }
            return BadRequest();
        }

       
        // PUT api/<AccountController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<AccountController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        private async Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            var user = await _userManager.FindByNameAsync(username);
            var result = await _signInManager.CheckPasswordSignInAsync(user, password, false);            
            var roles = await _userManager.GetRolesAsync(user);
      
            if (user != null && result.Succeeded && user.isEnabled)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, roles.FirstOrDefault()),
                    new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            // if user not found
            return null;
        }

    }
}
