﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        readonly IReviewService _reviewService;
        private readonly UserManager<User> _userManager;

        public ReviewsController(IReviewService service, UserManager<User> userManager)
        {
            _reviewService = service;
            _userManager = userManager;
        }

        // GET: api/reviews
        [HttpGet]
        public ActionResult<IEnumerable<ReviewModel>> GetAll()
        {
            var reviews = _reviewService.GetAll();
            return Ok(reviews);
        }

        // GET api/reviews/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReviewModel>> GetById(int id)
        {
            var reviews = await _reviewService.GetByIdAsync(id);
            return Ok(reviews);
        }

        // POST api/reviews
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] ReviewModel reviewModel)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            reviewModel.UserId = user.Id;
            var response = ValidateReview(reviewModel);
            if (response != String.Empty) { return BadRequest(new { response = response }); }
            reviewModel.Created = DateTime.Now;
            await _reviewService.AddAsync(reviewModel);
            return Ok();
        }

        // PUT api/reviews/5
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult> Update([FromBody] ReviewModel reviewModel)
        {
            var response = ValidateReview(reviewModel);
            if (response != String.Empty) { return BadRequest(new { response = response }); }
            await _reviewService.UpdateAsync(reviewModel);

            return Ok();
        }

        // DELETE api/reviews/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _reviewService.DeleteByIdAsync(id);
            return Ok();
        }

        private string ValidateReview(ReviewModel reviewModel)
        {
            var response = String.Empty;
            if (String.IsNullOrEmpty(reviewModel.Text)) { response = "Text cannot be empty"; }
            else if (reviewModel.UserId == default) { response = "Wrong User Id"; }
            return response;
        }


    }
}
