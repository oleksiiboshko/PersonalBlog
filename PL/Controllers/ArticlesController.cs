﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        readonly IArticleService _articleService;

        public ArticlesController(IArticleService service)
        {
            _articleService = service;
        }


        // GET: api/<ArticlesController>
        [HttpGet]
        public ActionResult<IEnumerable<ArticleModel>> GetAll()
        {
            var articles = _articleService.GetAll();
            return Ok(articles);
        }

        // GET api/<ArticlesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ArticleModel>> GetById(int id)
        {
            var article = await _articleService.GetByIdAsync(id);
            return Ok(article);
        }

        // GET api/articles/5
        [HttpGet("{articleId}/reviews")]
        public async Task<ActionResult<IEnumerable<ReviewModel>>> GetReviewsByArticleId(int articleId)
        {
            var reviews = await _articleService.GetReviewsByArticleIdAsync(articleId);
            return Ok(reviews);
        }

        // POST api/articles
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] ArticleModel articleModel)
        {
            var response = ValidateArticle(articleModel);
            if (!String.IsNullOrEmpty(response)) { return BadRequest(new { response = response }); }
            articleModel.Created = DateTime.Now;
            await _articleService.AddAsync(articleModel);
            return Ok();
        }

        // PUT api/articles/5
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult> Update([FromBody] ArticleModel articleModel)
        {
            var response = ValidateArticle(articleModel);
            if (!String.IsNullOrEmpty(response)) { return BadRequest(new { response = response }); }
            await _articleService.UpdateAsync(articleModel);
            return Ok();
        }

        // DELETE api/articles/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _articleService.DeleteByIdAsync(id);
            return Ok();
        }

        private string ValidateArticle(ArticleModel articleModel)
        {
            var response = String.Empty;
            if (String.IsNullOrEmpty(articleModel.Title)) { response = "Title cannot be empty"; }
            else if (String.IsNullOrEmpty(articleModel.Text)) { response = "Title cannot be empty";}
            else if (articleModel.BlogId == default) { response = "Wrong blogId"; }
            return response;
        }
    }
}
