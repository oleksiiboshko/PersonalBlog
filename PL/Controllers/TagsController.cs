﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TagsController : ControllerBase
    {
        readonly ITagService _tagService;

        public TagsController(ITagService service)
        {
            _tagService = service;
        }
        // GET: api/tags
        [Authorize]
        [HttpGet]
        public ActionResult<IEnumerable<TagModel>> GetAll()
        {
            var tags = _tagService.GetAll();
            return Ok(tags);
        }

        // GET api/tags/article/5
        [HttpGet("article/{articleId}")]
        public ActionResult<IEnumerable<TagModel>> GetTagsByArticleId(int articleId)
        {
            var tags = _tagService.GetTagsByArticleId(articleId);
            return Ok(tags);
        }

        // POST api/tags
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] TagModel tagModel)
        {
            if (String.IsNullOrEmpty(tagModel.Name)) return BadRequest(new { response = "Title of tag cannot be empty" });

            tagModel.isEnabled = true;

            await _tagService.AddAsync(tagModel);

            return Ok();
        }

        // POST api/tags/article
        [Authorize]
        [HttpPost("article")]
        public ActionResult AddTagsToArticle([FromBody] JsonElement body)
        {
            string jsonString = System.Text.Json.JsonSerializer.Serialize(body);
            var data = JObject.Parse(jsonString);
            var properties = data.Properties();
            var title = properties.Last().Value.ToString();
            var tagsJson = properties.First().Value;

            var tagModels = tagsJson.ToObject<TagModel[]>();
            _tagService.AddTagsToArticle(tagModels, title);
            return Ok();
        }



        // DELETE api/tags/5
        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _tagService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
