﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class BlogsController : ControllerBase
    {
        private readonly IBlogService _blogService;
        private readonly UserManager<User> _userManager;


        public BlogsController(IBlogService service, UserManager<User> userManager)
        {
            _blogService = service;
            _userManager = userManager;
        }

        // GET: api/blog
        [HttpGet]
        public ActionResult<IEnumerable<BlogModel>> GetAll()
        {
            return Ok(_blogService.GetAll());
        }

        // GET api/blogs/myblogs
        [Authorize]
        [HttpGet("myblogs")]
        public async Task<ActionResult<BlogModel>> GetBlogsOfCurrentUser()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var blogs = _blogService.GetBlogsByUserId(user.Id);
            return Ok(blogs);
        }

        // GET api/BlogController/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BlogModel>> GetById(int id)
        {
            var blog = await _blogService.GetByIdAsync(id);
             return Ok(blog);
        }

        // GET api/BlogController/5/articles
        [HttpGet("{id}/articles")]
        public ActionResult<IEnumerable<ArticleModel>> GetArticlesByBlogId(int id)
        {
            var articles = _blogService.GetArticlesByBlogId(id);
            return Ok(articles);
        }

        
        // POST api/blogs
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] BlogModel blogModel)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            blogModel.UserId = user.Id;
            var response = ValidateBlog(blogModel);
            if (!String.IsNullOrEmpty(response)) { return BadRequest(new { response = response }); }
            blogModel.Created = DateTime.Now;
            await _blogService.AddAsync(blogModel);
            return Ok();
        }

        // PUT api/BlogController/5
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult> Update([FromBody] BlogModel blogModel)
        {
            var response = ValidateBlog(blogModel);
            if (!String.IsNullOrEmpty(response)) { return BadRequest(new { response = response });}

            await _blogService.UpdateAsync(blogModel);
            return Ok();
        }

        // DELETE api/BlogController/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _blogService.DeleteByIdAsync(id);
            return Ok();
        }

        private string ValidateBlog(BlogModel blogModel)
        {
            var response = String.Empty;
            if (String.IsNullOrEmpty(blogModel.Title)) { response = "Title cannot be empty"; }
            else if (blogModel.UserId == default) { response = "Wrong User Id"; }
            return response;
        }

    }
}
