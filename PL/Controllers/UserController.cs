﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly UserManager<User> _userManager;

        public UsersController(IUserService service, UserManager<User> userManager)
        {
            _userManager = userManager;
            _userService = service;
        }

        // GET: api/<UserController>
        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult<IEnumerable<UserModel>> GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }

        // GET api/users/5
        [Authorize(Roles = "admin")]
        [HttpGet("{id}")]
        public async Task<ActionResult<UserModel>> GetById(int id)
        {
            var user = await _userService.GetByIdAsync(id);

            return Ok();
        }

        // GET api/users/current
        [Authorize]
        [HttpGet("current")]
        public async Task<ActionResult<UserModel>> GetCurrentUser()
        {
            var username = User.Identity.Name;
            var currentUser = await _userManager.FindByNameAsync(username);
            var roles = await _userManager.GetRolesAsync(currentUser);
            return Ok(new {
                Id = currentUser.Id,
                username = currentUser.UserName,
                IsEnabled = true,
                role = roles.FirstOrDefault()
            }) ;
        }


        // GET api/users/5
        [Authorize(Roles = "admin")]
        [HttpGet("{id}/blogs")]
        public ActionResult<IEnumerable<BlogModel>> GetBlogsByUserId(int userId)
        {
            var users = _userService.GetBlogsByUserId(userId);

            return Ok();
        }


        // PUT api/users/ban/5
        [Authorize(Roles = "admin")]
        [HttpPut("ban/{userId}")]
        public async Task<ActionResult> BanUser(int userId)
        {
            await _userService.BanUser(userId);

            return Ok();
        }


    }
}
