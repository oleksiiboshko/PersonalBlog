﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace PL
{
    public class AuthOptions
    {
        // token publisher
        public const string ISSUER = "WebApplicationwWebAPI_JWT_demoServer";
        // token user
        public const string AUDIENCE = "WebApplicationwWebAPI_JWT_demoClient";
        const string KEY = "mysupersecret_secretkey!123"; // key for encrypting
        public const int LIFETIME = 30; // token ttl - 1 minute
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }

    }
}
