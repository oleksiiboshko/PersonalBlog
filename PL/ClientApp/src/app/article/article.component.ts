import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Article } from '../article';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  articles: Article[] = [];


  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleService,
  ) { }

  ngOnInit() {
    this.getArticles();
  }

  getArticles(): void {
    this.articleService.getArticles()
    .pipe(
      map((data) => {
        data.forEach(function (article) {
          article.created = new Date(article.created);
          console.log(article.articleId);
        });
        return data;
      })).
      subscribe(articles => {
        this.articles = articles;
        
      });
    console.log(this.articles);
  }


}
