import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Review } from './review';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private reviewsUrl = 'api/articles';
  private makeReviewUrl = 'api/reviews';

  token = localStorage.getItem("jwt");

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      "Accept": "application/json",
      "Authorization": "Bearer " + this.token
    })
  };
    route: any;
  

  constructor(
    private http: HttpClient
  ) { }

  getReviewsByArticleId(id: number): Observable<Review[]> {
    const url = `${this.reviewsUrl}/${id}/reviews`;
    return this.http.get<Review[]>(url);
  }

  addReview(review: Object): Observable<Object> {
    const credentials = JSON.stringify(review);
    return this.http.post(this.makeReviewUrl, credentials, this.httpOptions);
  }

  deleteReview(id: number): Observable<Object> {
    const url = `${this.makeReviewUrl}/${id}`;
    return this.http.delete(url, this.httpOptions);
  }
}
