import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Article } from '../article';
import { Review } from '../review';
import { Location } from '@angular/common';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  article: Article | undefined;

  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleService,
    public datepipe: DatePipe,
    private location: Location
  ) { }

  ngOnInit() {
    this.getArticle();
  }

  getArticle(): void {
    const id = Number(this.route.snapshot.paramMap.get('articleId'));
    console.log("----", id);
    this.articleService.getArticle(id).
      pipe(
        map(article => {
          article.created = new Date(article.created);
          return article;
        })
    )
      .subscribe(article => { this.article = article; });
  }
  goBack(): void {
    this.location.back();
  }
}
