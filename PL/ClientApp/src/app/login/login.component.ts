import { Component, Injectable, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ReviewComponent } from '../review/review.component';
import { AccountComponent } from '../account/account.component';
import { UserBlogComponent } from '../user-blog/user-blog.component';
import { ReviewService } from '../review.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  authForm = this.formBuilder.group({
    username: '',
    password: ''
  });


  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) {
    this.loginForm = new FormGroup({

      "username": new FormControl("", Validators.required),
      "password": new FormControl("", Validators.required)
    })
  }

  ngOnInit() {

  }

  onSubmit(): void {
    this.accountService.LogIn(this.loginForm);
  }
}
