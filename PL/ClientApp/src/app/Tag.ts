import { Article } from "./article";

export interface Tag {
  tagId: number;
  name: string;
  isEnabled: boolean;
}
