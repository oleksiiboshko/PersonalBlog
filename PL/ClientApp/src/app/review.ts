export interface Review {
  id: number;
  text: string;
  created: Date;
  userId: number;
  articleId: number;
}
