import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, NgForm, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AccountService } from '../account.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [AccountService]
})
export class SignupComponent implements OnInit {
  
  signupForm: FormGroup;

  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder
  ) {
    this.signupForm = new FormGroup({

      "username": new FormControl("", Validators.required),
      "password": new FormControl("", Validators.required),
      "confirmPassword": new FormControl("", [Validators.required, Validators.minLength(8)])
    });
  }

  ngOnInit() {
  }

  confirmPasswords(): ValidationErrors {
    return (control: AbstractControl): ValidationErrors | null => {
      const password = this.signupForm.value["password"];
      const confirmPassword = this.signupForm.value["confirmPassword"];
      return password === confirmPassword ? {DontMatch: true}: null;
    };
  }
  

  onSubmit(): void {

    this.accountService.signUp(this.signupForm);

  }

}
