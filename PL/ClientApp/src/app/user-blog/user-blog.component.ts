import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Blog } from '../blog';
import { BlogService } from '../blog.service';
import { Tag } from '../Tag';
import { TagService } from '../tag.service';

@Component({
  selector: 'app-user-blog',
  templateUrl: './user-blog.component.html',
  styleUrls: ['./user-blog.component.css']
})
export class UserBlogComponent implements OnInit {

  blogs: Blog[] = [];
  tagsToShow: Tag[] = [];
  tagsToSend: Tag[] = [];
  blogForm: FormGroup;
  articleForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private blogService: BlogService,
    private tagService: TagService,
    public datepipe: DatePipe
    ) {
    this.blogForm = new FormGroup({
      "title": new FormControl("", Validators.required)
    });
    this.articleForm = new FormGroup({
      "title": new FormControl("", Validators.required),
      "text": new FormControl("", Validators.required),
      "blogId": new FormControl("", Validators.required)
    });
  }

  ngOnInit() {
    this.getBlogsByCurrentUser();
    this.getTags();
  }

  getBlogsByCurrentUser(): void {
    this.blogService.getBlogsByCurrentUser()
      .pipe(
        map((data) => {
          data.forEach(function (blog) {
            blog.created = new Date(blog.created);
          });
          return data;
        }))
      .subscribe(blogs => { this.blogs = blogs; });
  }

  getTags(): void {
    this.tagService.getAllTags()
      .subscribe(tags => this.tagsToShow = tags);
  }
  addTag(tag: Tag) {
    if (!this.containsObject(tag)) {
      this.tagsToSend.push(tag);
    }
    else {
      const index = this.tagsToShow.indexOf(tag, 0);
      this.tagsToShow.splice(index, 1);
    }
    
  }

  containsObject(tag: Tag): boolean {
    for (let tagToCompare of this.tagsToSend) {
      if (tag === tagToCompare) {
        return true;
      }
    }
    return false;
  }

  addBlog(): void {

    this.blogService.AddBlogToCurrentUser(this.blogForm).subscribe(() => { this.ngOnInit(); });

  }

  addArticle(): void {
    this.blogService.AddArticle(this.articleForm, this.tagsToSend);
  }

  deleteBlog(id: number): void {
    this.blogService.deleteBlog(id);
    this.ngOnInit();
  }
}
