import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Article } from './article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private articlesUrl = 'api/articles';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient
  ) { }

  getArticle(id: number): Observable<Article> {
    const url = `${this.articlesUrl}/${id}/`;
    return this.http.get<Article>(url);
  }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.articlesUrl, this.httpOptions);
  }
}
