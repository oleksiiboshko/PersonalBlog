import {  Component } from '@angular/core';

import { AccountService } from '../account.service';


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;


  constructor(
    private accountService: AccountService) { }
  ngOnInit() {
  }

  isLoggedIn() {
    if (localStorage.getItem("jwt") != null) {
      return true;
    }
    return false;
  }

  LogOut() {
    this.accountService.logOut();

  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
