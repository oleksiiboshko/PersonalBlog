
export interface Blog {
  id: number;
  title: string;
  created: Date;
  articles: number[];
}
