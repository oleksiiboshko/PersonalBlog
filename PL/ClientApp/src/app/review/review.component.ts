import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from '../account.service';
import { Review } from '../review';
import { ReviewService } from '../review.service';
import { User } from '../User';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  reviews: Review[] = [];
  currentUser: User;
  id: number;
  inputControl: FormControl;


  myForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private reviewService: ReviewService,
    private accountService: AccountService,
    public datepipe: DatePipe,
  ) {
    this.id = route.snapshot.params['articleId'];
    this.myForm = new FormGroup({
      "text": new FormControl("", Validators.required)
    })
  }

  ngOnInit() {
    this.getReviewsByArticleId();
    this.getCurrentUser();
  }
  getReviewsByArticleId(): void {
    const id = Number(this.route.snapshot.paramMap.get('articleId'));
    this.reviewService.getReviewsByArticleId(id).subscribe(reviews => { this.reviews = reviews;});
  }

  onSubmit(): void {
    const id = Number(this.route.snapshot.paramMap.get('articleId'));
    const credentials: Object = { articleId: id, text: this.myForm.value["text"] };
    this.reviewService.addReview(credentials).subscribe(() => { this.ngOnInit() });
  }

  getCurrentUser(): void {
    this.accountService.getCurrentUser().subscribe(user => { this.currentUser = user; });
  }

  deleteReview(id: number): void {
    this.reviewService.deleteReview(id).subscribe(() => { this.ngOnInit(); });
  }

  isLoggedIn() {
    if (localStorage.getItem("jwt") != null) {
      return true;
    }
    return false;
  }

}
