
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Blog } from './blog';
import { Article } from './article';
import { FormGroup, NgForm } from '@angular/forms';
import { Tag } from './Tag';


@Injectable({
  providedIn: 'root'
})
export class BlogService {
  

  private blogsUrl = 'api/blogs';
  private myBlogsUrl = 'api/blogs/myblogs';
  private addArticleUrl = 'api/articles';
  private addTagsToArticleUrl = 'api/tags/article/'

  token = localStorage.getItem("jwt");


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      "Accept": "application/json",
      "Authorization": "Bearer " + this.token})
  };

  constructor(
    private http: HttpClient
  ) { }

  getBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>(this.blogsUrl, this.httpOptions);
  }

  getBlog(id: number): Observable<Blog> {
    const url = `${this.blogsUrl}/${id}`;
    return this.http.get<Blog>(url);
  }

  getArticlesByBlogId(id: number): Observable<Article[]> {
    const url = `${this.blogsUrl}/${id}/articles`;
    return this.http.get<Article[]>(url, this.httpOptions);
  }

  getBlogsByCurrentUser(): Observable<Blog[]> {
    return this.http.get<Blog[]>(this.myBlogsUrl, this.httpOptions);
  }

  AddBlogToCurrentUser(form: FormGroup) {
    const credentials = JSON.stringify(form.value);
    return this.http.post(this.blogsUrl, credentials, this.httpOptions);
  }

  AddArticle(form: FormGroup, tags: Tag[]) {
    form.value['blogId'] = Number(form.value['blogId']);
    for (let tag of tags) {
      tag.tagId = Number(tag.tagId);
    }
    const articleCredentials = JSON.stringify(form.value);
    const title = form.value['title'];
    const tagCredentials = JSON.stringify({ tags: tags, title: title });
    this.http.post(this.addArticleUrl, articleCredentials, this.httpOptions).subscribe(() => {

      this.http.post(this.addTagsToArticleUrl, tagCredentials, this.httpOptions).subscribe();
    });
  }

  deleteBlog(id: number): void {
    const url = `${this.blogsUrl}/${id}`;
    this.http.delete(url, this.httpOptions).subscribe();
  }
}
