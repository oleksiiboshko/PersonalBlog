export interface Article {
  articleId: number;
  title: string;
  text: string;
  created: Date;
  blogId: number;
}

