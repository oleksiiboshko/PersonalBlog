import { DatePipe } from '@angular/common';
import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { Blog } from '../blog';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers: [BlogService, DatePipe]
})
export class BlogComponent implements OnInit {

  blogs: Blog[] = [];

  constructor(private blogService: BlogService, public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.getBlogs();
  }

  getBlogs(): void {
    this.blogService.getBlogs().
      pipe(
        map((data) => {
          data.forEach(function (blog) {
            blog.created = new Date(blog.created);
          });
          return data;
        })).
      subscribe(blogs => {
        this.blogs = blogs;
      });
  }
  
}
