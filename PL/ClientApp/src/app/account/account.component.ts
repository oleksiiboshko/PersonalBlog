import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { FormBuilder, NgForm } from '@angular/forms';
import { User } from '../User';
import { map } from 'rxjs/operators';
import { Tag } from '../Tag';
import { TagService } from '../tag.service';
import { ReviewComponent } from '../review/review.component';
import { UserBlogComponent } from '../user-blog/user-blog.component';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: [AccountService]
})
export class AccountComponent implements OnInit {

  role: string;
  currentUser: User;
  users: User[] = [];
  enabledTags: Tag[] = [];
  disabledTags: Tag[] = [];

  constructor(
    private accountService: AccountService,
    private tagService: TagService,
  ) { }

  ngOnInit() {
    this.getCurrentUser();   
    this.getUsers();
    this.enabledTags = [];
    this.disabledTags = [];
    this.getTags();

    
  }

  getCurrentUser(): void {
    this.accountService.getCurrentUser()
      .pipe(map((user) => {
        user.isEnabled = undefined;
        return user;
      }))
      .subscribe(user => { this.currentUser = user; });
  }

  getUsers(): void {
    this.accountService.getUsers()
      .pipe(map((users) => {
        users.forEach(function (user) {
          user.role = undefined;
        });
        return users;
      }))
      .subscribe(users => this.users = users);
  }

  getTags(): void {
    this.tagService.getAllTags().subscribe(tags => tags.forEach((tag) => {
      if (tag.isEnabled) { this.enabledTags.push(tag); }
      else { this.disabledTags.push(tag); }
    }));
  }

  banUser(id: number): void {
    this.accountService.banUser(id).subscribe(() => { this.ngOnInit();});

  }

  deleteTag(id: number): void {
    this.tagService.deleteTag(id).subscribe(() => { this.ngOnInit(); });

  }

  addTag(form: NgForm): void {
    this.tagService.addTag(form).subscribe(() => { this.ngOnInit(); });

  }

}
