import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Tag } from './tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  private tagsUrl = 'api/tags';

  token = localStorage.getItem("jwt");

  httpAuthorizedOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      "Accept": "application/json",
      "Authorization": "Bearer " + this.token

    })
  };

  constructor(
    private http: HttpClient
  ) { }

  getTagsByArticleId(id: number): Observable<Tag[]> {
    const url = `${this.tagsUrl}/article/${id}`;
    console.log("url: ", url);
    return this.http.get<Tag[]>(url);
  }

  getAllTags(): Observable<Tag[]> {
    const url = `${this.tagsUrl}`;
    return this.http.get<Tag[]>(url, this.httpAuthorizedOptions);
  }

  addTag(form: NgForm): Observable<Object> {
    const credentials = JSON.stringify(form.value);
    return this.http.post(this.tagsUrl, credentials, this.httpAuthorizedOptions);
  }
  deleteTag(id: number): Observable<Object> {
    const url = `${this.tagsUrl}/${id}`;
    return this.http.delete(url, this.httpAuthorizedOptions);
  }
}
