import { Component, OnInit } from '@angular/core';
import { Tag } from '../Tag';
import { TagService } from '../tag.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  tags: Tag[] = [];

  constructor(
    private tagService: TagService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.getTagsByArticleId();
  }

  getTagsByArticleId(): void {
    const id = Number(this.route.snapshot.paramMap.get('articleId'));
    this.tagService.getTagsByArticleId(id)
      .pipe(map((tags) => {
        console.log("here",tags);
        tags.forEach(function (tag) {
          console.log(tag);
          tag = { tagId: tag.tagId, name: tag.name, isEnabled: undefined };
          console.log(tag);
        })
        return tags;
      }))
      .subscribe(tags => { this.tags = tags;});
    
  }

}
