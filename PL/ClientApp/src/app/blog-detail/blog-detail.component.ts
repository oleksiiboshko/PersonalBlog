import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { Blog } from '../blog'
import { DatePipe } from '@angular/common';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Article } from '../article';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {

  blog: Blog | undefined;
  articles: Article[] = [];

  constructor(
    private route: ActivatedRoute,
    private blogService: BlogService,
    public datepipe: DatePipe,
    private location: Location
  ) { }

  ngOnInit() {
    this.getBlog();
    this.getArticlesByBlogId();
  }

  getBlog(): void {
    const id = Number(this.route.snapshot.paramMap.get('blogId'));
    this.blogService.getBlog(id).
    pipe(
      map(blog => {
        blog.created = new Date(blog.created);
        return blog;
        }))
      .subscribe(blog => { this.blog = blog; });
  }
  getArticlesByBlogId(): void {
    const id = Number(this.route.snapshot.paramMap.get('blogId'));
    this.blogService.getArticlesByBlogId(id)
      .pipe(
        map((data) => {
          data.forEach(function (article) {
            article.created = new Date(article.created);
          });
          return data;
        })).
      subscribe(articles => { this.articles = articles; });
  }

  goBack(): void {
    this.location.back();
  }
}
