import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Tag } from './Tag';
import { User } from './User';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private loginUrl = 'api/account/login';
  private signupUrl = 'api/account/signup';
  private userInfo = 'api/users/current';
  private userUrl = 'api/users';

  token = localStorage.getItem("jwt");

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  httpAuthorizedOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      "Accept": "application/json",
      "Authorization": "Bearer " + this.token
      
    })
  };

  constructor(
    private router: Router,
    private http: HttpClient,

  ) {
    
  }

  LogIn(form: FormGroup): void {
    const credentials = JSON.stringify(form.value);
    this.http.post(this.loginUrl, credentials, this.httpOptions)
    .subscribe(response => {
      const token = (<any>response).access_token;
      localStorage.setItem("jwt", token);
       
    }, err => {
        alert(err.error['response']);
    });

  }


  changeValue(booleanValue) {
    booleanValue.authorized = !booleanValue.authorized;
  }

  logOut() {
    localStorage.removeItem("jwt");
    this.router.navigate(["/login"]);
    window.location.reload();
    
  }

  signUp(form: FormGroup): void {
    const credentials = JSON.stringify(form.value);
    this.http.post(this.signupUrl, credentials, this.httpOptions)
      .subscribe(response => {
      const token = (<any>response).access_token;
      localStorage.setItem("jwt", token);
      this.router.navigate(["/login"]);
      }, err => {
        if (err.error['response'] != undefined)
          alert(err.error['response']);
    });
  }

  getCurrentUser(): Observable<User> {
    return this.http.get<User>(this.userInfo, this.httpAuthorizedOptions);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl, this.httpAuthorizedOptions);
  }

  banUser(id: number): Observable<Object> {
    console.log(id);
    const url = `${this.userUrl}/ban/${id}`;
    return this.http.put(url, this.httpOptions);
  }
}
