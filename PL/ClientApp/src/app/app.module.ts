import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { BlogComponent } from './blog/blog.component';
import { DatePipe } from '@angular/common';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { ArticleComponent } from './article/article.component';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { ReviewComponent } from './review/review.component';
import { TagComponent } from './tag/tag.component';
import { AccountComponent } from './account/account.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { UserBlogComponent } from './user-blog/user-blog.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    BlogComponent,
    UserBlogComponent,
    BlogDetailComponent,
    ArticleComponent,
    ArticleDetailComponent,
    ReviewComponent,
    TagComponent,
    AccountComponent,
    SignupComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: BlogComponent, pathMatch: 'full'},
      //      { path: 'blogs', component: BlogComponent},
      { path: 'articles', component: ArticleComponent },
      { path: 'myblogs', component: UserBlogComponent },
      { path: 'account', component: AccountComponent },
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupComponent },
      { path: 'blogs/:blogId', component: BlogDetailComponent },
      { path: 'myblogs/api/blogs/:blogId', component: BlogDetailComponent },
      { path: 'blogs/:blogId/articles/:articleId', component: ArticleDetailComponent },
      { path: 'myblogs/api/blogs/:blogId/articles/:articleId', component: ArticleDetailComponent },
      { path: 'articles/api/articles/:articleId', component: ArticleDetailComponent },
      { path: '**', component: BlogComponent }
    ])
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
