﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Enums
{
    public enum TagEnum
    {
        Travel = 1,
        Science,
        Sport,
        Art,
        Photograph,
        Gaming
    }
}
