﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IBlogRepository : IRepository<Blog>
    {
        IQueryable<Blog> FindAllWithDetails();

        Task<Blog> GetByIdWithDetailsAsync(int id);

        void DeleteByIdAsync(int id);
    }
}
