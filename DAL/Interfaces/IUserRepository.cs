﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        IQueryable<User> FindAllWithDetails();

        Task<User> GetByIdWithDetailsAsync(int id);

        Task<string> GetUserRoleByIdAsync(int id);

        void BanUser(int id);

        Task DeleteByIdAsync(int id);
    }
}
