﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IReviewRepository : IRepository<Review>
    {
        IQueryable<Review> FindAllWithDetails();

        Task<IQueryable<Review>> GetAllByArticleIdWithDetailsAsync(int id);

        void DeleteByIdAsync(int id);
    }
}
