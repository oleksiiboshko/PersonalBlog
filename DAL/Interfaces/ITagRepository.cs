﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ITagRepository : IRepository<Tag>
    {
        IQueryable<ArticleTag> GetTagsByArticleId(int id);

        void AddTagsToArticle(Tag[] tags, string title);

        void DisableTag(int id);
    }
}
