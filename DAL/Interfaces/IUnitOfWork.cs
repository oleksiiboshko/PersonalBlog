﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IArticleRepository ArticleRepository { get; }

        IBlogRepository BlogRepository { get; }

        IReviewRepository ReviewRepository { get; }

        IUserRepository UserRepository { get; }

        ITagRepository TagRepository { get; }

        Task SaveAsync();
    }

}
