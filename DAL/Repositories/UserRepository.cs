﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly PersonalBlogDbContext _context;

        public UserRepository(PersonalBlogDbContext context)
        {
            _context = context;
           
            
        }

        public IQueryable<User> FindAllWithDetails()
        {
            return _context.Users.Include(u => u.UserBlogs);
        }

        public Task<User> GetByIdWithDetailsAsync(int id)
        {
            return Task.Run(()=>_context.Users.FirstOrDefault(u => u.Id == id));
        }

        public IQueryable<User> FindAll()
        {
            return _context.Users;
        }

        public Task<User> GetByIdAsync(int id)
        {
            return Task.Run(() => _context.Users.FirstOrDefault(u => u.Id == id));
        }

        public Task<string> GetUserRoleByIdAsync(int id)
        {
            var roleId = _context.UserRoles.FirstOrDefault(u => u.UserId == id).RoleId;
            return Task.Run(() => _context.Roles.FirstOrDefault(u => u.Id == roleId).Name);
        }

        public Task AddAsync(User entity)
        {
            return Task.Run(() => _context.Users.Add(entity));
        }

        public void Update(User entity)
        {
            _context.Users.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(User entity)
        {
            _context.Users.Remove(entity);
            _context.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            var userBlogs = _context.Blogs.Where(x => x.UserId == id);
            _context.Blogs.RemoveRange(userBlogs);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
           

        }

        public void BanUser(int id)
        {
            var user = _context.Users.Find(id);
            if (user.isEnabled)
                user.isEnabled = false;
            else
                user.isEnabled = true;

            _context.Users.Update(user);
            _context.SaveChanges();
        }
    }
}
