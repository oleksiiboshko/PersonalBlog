﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly PersonalBlogDbContext _context;

        public ArticleRepository(PersonalBlogDbContext context)
        {
            _context = context;
        }

        public IQueryable<Article> FindAllWithDetails()
        {
            return _context.Articles.Include(a => a.Tags).Include(a => a.ArticleReviews);
        }

        public Task<Article> GetByIdWithDetailsAsync(int id)
        {
            return Task.Run(()=>_context.Articles.Include(a => a.Tags).Include(a => a.ArticleReviews).FirstOrDefault(a => a.ArticleId==id));
        }

        public IQueryable<Article> FindAll()
        {
            return _context.Articles;
        }

        public async Task<Article> GetByIdAsync(int id)
        {
            return await _context.Articles.FindAsync(id);
        }

        public async Task AddAsync(Article entity)
        {
            await _context.Articles.AddAsync(entity);
            _context.SaveChanges();
        }

        public void Update(Article entity)
        {
            _context.Articles.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(Article entity)
        {
            _context.Articles.Remove(entity);
            _context.SaveChanges();
        }

        public void DeleteByIdAsync(int id)
        {
            var itemToRemove = _context.Articles.FirstOrDefault(a => a.ArticleId == id);

            _context.Articles.Remove(itemToRemove);

            _context.SaveChanges();
        }
    }
}
