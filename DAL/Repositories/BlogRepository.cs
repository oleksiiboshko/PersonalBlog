﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class BlogRepository : IBlogRepository
    {
        private readonly PersonalBlogDbContext _context;

        public BlogRepository(PersonalBlogDbContext context)
        {
            _context = context;
        }

        public IQueryable<Blog> FindAllWithDetails()
        {
            return _context.Blogs.Include(blog => blog.BlogArticles);
        }

        public async Task<Blog> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(()=> _context.Blogs.Include(blog => blog.BlogArticles).FirstOrDefault(blog => blog.Id == id));
        }

        public IQueryable<Blog> FindAll()
        {
            return _context.Blogs;
        }

        public async Task<Blog> GetByIdAsync(int id)
        {
            return await _context.Blogs.FindAsync(id);
        }

        public Task AddAsync(Blog entity)
        {
            return Task.Run(() =>{ _context.Blogs.Add(entity); _context.SaveChanges(); });
        }

        public void Update(Blog entity)
        {
            _context.Blogs.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(Blog entity)
        {
            _context.Blogs.Remove(entity);
            _context.SaveChanges();

        }

        public void DeleteByIdAsync(int id)
        {

            
            var blogArticles = _context.Articles.Where(x => x.BlogId == id);
            foreach (var article in blogArticles)
            {
                var articleTags = _context.ArticleTags.Where(t => t.ArticleId == article.ArticleId);
                _context.ArticleTags.RemoveRange(articleTags);
            }
            _context.Articles.RemoveRange(blogArticles);
            var blog = _context.Blogs.Find(id);

            _context.Blogs.Remove(blog);
            _context.SaveChanges();

        }
    }
}
