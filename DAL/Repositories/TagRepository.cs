﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class TagRepository : ITagRepository
    {
        private readonly PersonalBlogDbContext _context;

        public TagRepository(PersonalBlogDbContext context)
        {
            _context = context;
        }

        public IQueryable<Tag> FindAll()
        {
            return _context.Tags;
        }

        public IQueryable<ArticleTag> GetTagsByArticleId(int id)
        {
            return _context.ArticleTags.Where(x=>x.ArticleId==id);
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            return await _context.Tags.FindAsync(id);
        }

        public async Task AddAsync(Tag entity)
        {
            await _context.Tags.AddAsync(entity);
            _context.SaveChanges();
        }

        public void AddTagsToArticle(Tag[] tags, string title)
        {
            
            var article = _context.Articles.FirstOrDefault(a=>a.Title == title);
            foreach (var tag in tags)
            {
                _context.ArticleTags.Add(new ArticleTag
                {
                    TagId = tag.TagId,
                    //Tag = tag,
                    ArticleId = article.ArticleId,
                    //Article = article
                });
            }
            _context.SaveChanges();
        }

        public void Update(Tag entity)
        {
            _context.Tags.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(Tag entity)
        {
            _context.Tags.Remove(entity);
            _context.SaveChanges();
        }

        public void DisableTag(int id)
        {
            var tag = _context.Tags.FirstOrDefault(a => a.TagId == id);

            tag.isEnabled = false;

            _context.Tags.Update(tag);
            _context.SaveChanges();
        }
    }
}
