﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly PersonalBlogDbContext _context;

        public ReviewRepository(PersonalBlogDbContext context)
        {
            _context = context;
        }

        public IQueryable<Review> FindAllWithDetails() 
        {
            return _context.Reviews;
        }

        public Task<IQueryable<Review>> GetAllByArticleIdWithDetailsAsync(int id)
        {
            return Task.Run(() => _context.Reviews.Where(c => c.Article.ArticleId == id));
        }

        public IQueryable<Review> FindAll() 
        {
            return _context.Reviews;
        }

        public Task<Review> GetByIdAsync(int id)
        {
            return Task.Run(() => _context.Reviews.FirstOrDefault(c => c.Id == id));
        }

        public Task AddAsync(Review entity)
        {
            return Task.Run(() => { _context.Reviews.Add(entity); _context.SaveChanges(); });
        }

        public void Update(Review entity)
        {
            _context.Reviews.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(Review entity)
        {
            _context.Reviews.Remove(entity);
            _context.SaveChanges();
        }

        public void DeleteByIdAsync(int id)
        {
            var itemToRemove = _context.Reviews.FirstOrDefault(u => u.Id == id);



            _context.Reviews.Remove(itemToRemove); 
            _context.SaveChanges(); 
            

        }
    }
}
