﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using DAL.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.Contexts
{
    public class PersonalBlogDbContext : IdentityDbContext<User, Role, int>
    {
        public PersonalBlogDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
            
        }

        public DbSet<Blog> Blogs { get; set; }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<ArticleTag> ArticleTags { get; set; }

        //public override DbSet<User> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ArticleTag>()
                .HasKey(bc => new { bc.ArticleId, bc.TagId});
            



            modelBuilder.Entity<Role>().HasData(
                new Role
                {
                    Id = 1,
                    Name = "admin"
                },
                new Role
                {
                    Id = 2,
                    Name = "user"
                }
            );
            modelBuilder.Entity<Blog>().HasData(
                new Blog
                {
                    UserId = 1,
                    Id = 1,
                    Title="Truskavets",
                    Created= new DateTime(1998,9,4),
                    
                }
                );
            modelBuilder.Entity<Article>().HasData(new Article {BlogId = 1, ArticleId = 4, Title = "Some Text", Text = "Text too" });

            modelBuilder.Entity<User>().HasData(new User { Id = 1, UserName = "SoomeName" });
            modelBuilder.Entity<Tag>().HasData(
                new Tag { TagId = 1, Name = "Travel" },
                new Tag { TagId = 2, Name = "Science" },
                new Tag { TagId = 3, Name = "Sport" },
                new Tag { TagId = 4, Name = "Art" },
                new Tag { TagId = 5, Name = "Photograph" },
                new Tag { TagId = 6, Name = "Gaming" },
                new Tag { TagId = 7, Name = "Nature" }
            );
            
            


        }
    }
}
