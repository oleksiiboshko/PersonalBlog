﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.Contexts;
using DAL.Interfaces;
using DAL.Repositories;

namespace DAL.EF
{
    public class UnitOfWork : IUnitOfWork
    {
        public PersonalBlogDbContext _context;

        private IBlogRepository _blogRepository;
        private IArticleRepository _articleRepository;
        private IReviewRepository _commentRepository;
        private IUserRepository _userRepository;
        private ITagRepository _tagRepository;

        public IBlogRepository BlogRepository
        {
            get
            {
                if (_blogRepository == null)
                    _blogRepository = new BlogRepository(_context);
                return _blogRepository;
            }
        }

        public IArticleRepository ArticleRepository
        {
            get
            {
                if (_articleRepository == null)
                    _articleRepository = new ArticleRepository(_context);
                return _articleRepository;
            }
        }

        public IReviewRepository ReviewRepository
        {
            get
            {
                if (_commentRepository == null)
                    _commentRepository = new ReviewRepository(_context);
                return _commentRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_context);
                return _userRepository;
            }
        }

        public ITagRepository TagRepository
        {
            get
            {
                if (_tagRepository == null)
                    _tagRepository = new TagRepository(_context);
                return _tagRepository;
            }
        }
        public UnitOfWork(PersonalBlogDbContext context)
        {
           _context = context;
        }

        public async Task SaveAsync()
        {
            
           await _context.SaveChangesAsync();
        }


    }
}
