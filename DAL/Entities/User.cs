﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace DAL.Entities
{
    public class User : IdentityUser<int>
    {
        [Key]
        public override int Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? BirthDate { get; set; }

        public bool isEnabled { get; set; }

        public ICollection<Blog> UserBlogs { get; set; }

        public ICollection<Review> UserReviews { get; set; }

        public User()
        {
            this.UserBlogs = new HashSet<Blog>();
            this.UserReviews = new HashSet<Review>();
        }
    }
}
