﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DAL.Enums;

namespace DAL.Entities
{
    public class Article
    {
        [Key]
        public int ArticleId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Title { get; set; }
        [Column(TypeName = "nvarchar(4000)")]
        public string Text { get; set; }
        [Column(TypeName = "int")]
        public int BlogId { get; set; }

        public Blog Blog { get; set; }
        
        public ICollection<ArticleTag> Tags { get; set; }

        public ICollection<Review> ArticleReviews { get; set; }

        public Article()
        {
            this.Tags = new HashSet<ArticleTag>();
            this.ArticleReviews = new HashSet<Review>();
        }
    }
}
