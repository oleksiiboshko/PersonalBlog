﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    public class Blog
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Title { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        public int? UserId { get; set; }

        public User User { get; set; }

        public ICollection<Article> BlogArticles { get; set; }

        public Blog()
        {
            this.BlogArticles = new HashSet<Article>();
        }
    }
}
