﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    public class Review
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string Text { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        public int ArticleId { get; set; }
        [Column("UserId")]
        public int? UserId { get; set; }

        public Article Article { get; set; }

        public User User { get; set; }
    }
}
