﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    public class Tag
    {
        [Key]
        public int TagId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Name { get; set; }

        public bool isEnabled { get; set; }

        public ICollection<ArticleTag> Articles { get; set; }

        public Tag()
        {
            this.Articles = new HashSet<ArticleTag>();
        }
    }
}
