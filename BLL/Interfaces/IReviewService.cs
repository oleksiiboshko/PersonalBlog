﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Models;
using DAL.Entities;

namespace BLL.Interfaces
{
    public interface IReviewService : ICrud<ReviewModel>
    {
        IEnumerable<ReviewModel> GetAllWithDetails();

        IEnumerable<ReviewModel> GetCommentsByArticleId(int articleId);
    }
}
