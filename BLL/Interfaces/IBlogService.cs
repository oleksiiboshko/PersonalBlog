﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Models;

namespace BLL.Interfaces
{
    public interface IBlogService : ICrud<BlogModel>
    {
        IEnumerable<BlogModel> GetAllWithDetails();

        IEnumerable<ArticleModel> GetArticlesByBlogId(int blogId);

        IEnumerable<BlogModel> GetBlogsByUserId(int userId);
    }
}
