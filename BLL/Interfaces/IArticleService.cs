﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;

namespace BLL.Interfaces
{
    public interface IArticleService : ICrud<ArticleModel>
    {
        IEnumerable<ArticleModel> GetAllWithDetails();

        Task<IEnumerable<ReviewModel>> GetReviewsByArticleIdAsync(int articleId);
    }
}
