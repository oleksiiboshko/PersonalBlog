﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;
using DAL.Entities;

namespace BLL.Interfaces
{
    public interface ITagService : ICrud<TagModel>
    {
        public IEnumerable<TagModel> GetTagsByArticleId(int articleId);

        void AddTagsToArticle(TagModel[] tagModels, string title);
    }
}
