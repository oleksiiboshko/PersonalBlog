﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;

namespace BLL.Interfaces
{
    public interface IUserService : ICrud<UserModel>
    {
        IEnumerable<UserModel> GetAllWithDetails();

        IEnumerable<BlogModel> GetBlogsByUserId(int id);

        Task BanUser(int userId);


    }
}
