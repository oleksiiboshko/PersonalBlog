﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class ReviewService : IReviewService
    {
        IUnitOfWork Database { get; set; }
        readonly IMapper _mapper;

        public ReviewService(IUnitOfWork uow, IMapper mapper)
        {
            _mapper = mapper;
            Database = uow;
        }

        public IEnumerable<ReviewModel> GetAllWithDetails()
        {
            var comments = Database.ReviewRepository.FindAllWithDetails();

            return _mapper.Map<IEnumerable<ReviewModel>>(comments);
        }

        public IEnumerable<ReviewModel> GetCommentsByArticleId(int articleId)
        {
            var comments = Database.ReviewRepository.GetAllByArticleIdWithDetailsAsync(articleId);

            return _mapper.Map<IEnumerable<ReviewModel>>(comments);
        }

        public IEnumerable<ReviewModel> GetAll()
        {

            var comments = Database.ReviewRepository.FindAll();
            return _mapper.Map<IEnumerable<ReviewModel>>(comments);
        }

        public Task<ReviewModel> GetByIdAsync(int id)
        {
            var comment = Database.ReviewRepository.GetByIdAsync(id);

            return Task.Run(() => _mapper.Map<ReviewModel>(comment));
        }

        public Task AddAsync(ReviewModel model)
        {
            var comment = _mapper.Map<Review>(model);

            return Task.Run(() => Database.ReviewRepository.AddAsync(comment));
        }

        public Task UpdateAsync(ReviewModel model)
        {
            var comment = _mapper.Map<Review>(model);

            return Task.Run(() => { Database.ReviewRepository.Update(comment); Database.SaveAsync(); });
        }

        public Task DeleteByIdAsync(int modelId)
        {
            return Task.Run(() => { Database.ReviewRepository.DeleteByIdAsync(modelId); Database.SaveAsync(); });
        }
    }
}
