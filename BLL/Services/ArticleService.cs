﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class ArticleService : IArticleService
    {
        IUnitOfWork Database { get; set; }
        readonly IMapper _mapper;

        public ArticleService(IUnitOfWork uow, IMapper mapper)
        {
            _mapper = mapper;
            Database = uow;
        }

        public IEnumerable<ArticleModel> GetAllWithDetails()
        {
            var books = Database.BlogRepository.FindAllWithDetails();

            return _mapper.Map<IEnumerable<ArticleModel>>(books);
        }

        public async Task<IEnumerable<ReviewModel>> GetReviewsByArticleIdAsync(int articleId)
        {
            var reviews = await Database.ReviewRepository.GetAllByArticleIdWithDetailsAsync(articleId);

            return _mapper.Map<IEnumerable<ReviewModel>>(reviews);
        }

        public IEnumerable<ArticleModel> GetAll() 
        {
            var articles = Database.ArticleRepository.FindAll();

            return _mapper.Map<IEnumerable<ArticleModel>>(articles);
        }

        public async Task<ArticleModel> GetByIdAsync(int id)
        {
            var article = await Database.ArticleRepository.GetByIdAsync(id);

            return _mapper.Map<ArticleModel>(article);
        }

        public Task AddAsync(ArticleModel model)
        {
            var article = _mapper.Map<Article>(model);

            return Task.Run(() => Database.ArticleRepository.AddAsync(article));
        }

        public Task UpdateAsync(ArticleModel model)
        {
            var article = _mapper.Map<Article>(model);

            return Task.Run(() => { Database.ArticleRepository.Update(article); Database.SaveAsync(); });
        }

        public Task DeleteByIdAsync(int modelId)
        {
            return Task.Run(() => { Database.ArticleRepository.DeleteByIdAsync(modelId); Database.SaveAsync(); });
        }
    }
}
