﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using DAL.Interfaces;
namespace BLL.Services
{
    public class TagService : ITagService
    {
        IUnitOfWork Database { get; set; }
        readonly IMapper _mapper;

        public TagService(IUnitOfWork uow, IMapper mapper)
        {
            _mapper = mapper;
            Database = uow;
        }

        public IEnumerable<TagModel> GetAll() 
        {
            var tags = Database.TagRepository.FindAll();

            return _mapper.Map<IEnumerable<TagModel>>(tags);
        }

        public IEnumerable<TagModel> GetTagsByArticleId(int articleId)
        {
            var tags = Database.TagRepository.GetTagsByArticleId(articleId).Select(at=>at.Tag);

            return _mapper.Map<IEnumerable<TagModel>>(tags);
        }

        public async Task<TagModel> GetByIdAsync(int id) 
        {
            var tag = await Database.TagRepository.GetByIdAsync(id);

            return _mapper.Map<TagModel>(tag);
        }
        public void AddTagsToArticle(TagModel[] tagModels, string title)
        {
            Tag[] tags = _mapper.Map<Tag[]>(tagModels);
            Database.TagRepository.AddTagsToArticle(tags, title);

        }

        public Task AddAsync(TagModel model) 
        {
            var tag = _mapper.Map<Tag>(model);

            return Task.Run(() => { Database.TagRepository.AddAsync(tag);});
        }

        public Task UpdateAsync(TagModel model) 
        {
            var tag = _mapper.Map<Tag>(model);

            return Task.Run(() => { Database.TagRepository.Update(tag); Database.SaveAsync(); });
        }

        public Task DeleteByIdAsync(int modelId)
        {
            return Task.Run(() => { Database.TagRepository.DisableTag(modelId); });
        }
    }
}
