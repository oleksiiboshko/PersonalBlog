﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class BlogService : IBlogService
    {
        IUnitOfWork Database { get; set; }
        readonly IMapper _mapper;

        public BlogService(IUnitOfWork uow, IMapper mapper)
        {
            _mapper = mapper;
            Database = uow;
        }

        public IEnumerable<BlogModel> GetAllWithDetails()
        {
            return _mapper.Map<IEnumerable<BlogModel>>(Database.BlogRepository.FindAllWithDetails());
        }

        public IEnumerable<ArticleModel> GetArticlesByBlogId(int blogId)
        {
            //return _mapper.Map<IEnumerable<ArticleModel>>(Database.ArticleRepository.FindAllWithDetails().Where(a => a.BlogId == blogId));
            var blogs = Database.ArticleRepository.FindAll().Where(a => a.BlogId == blogId);
            return _mapper.Map<IEnumerable<ArticleModel>>(blogs);
        }

        public IEnumerable<BlogModel> GetAll()
        {
            var blogs = Database.BlogRepository.FindAll();

            return _mapper.Map<IEnumerable<BlogModel>>(blogs);
        }

        public async Task<BlogModel> GetByIdAsync(int id)
        {
            var blog = await Database.BlogRepository.GetByIdAsync(id);

            return _mapper.Map<BlogModel>(blog);
        }

        public IEnumerable<BlogModel> GetBlogsByUserId(int userId)
        {
            var blogs = Database.BlogRepository.FindAll().Where(b=>b.UserId==userId);

            return _mapper.Map<IEnumerable<BlogModel>>(blogs);
        }

        public Task AddAsync(BlogModel model)
        {
            var blog = _mapper.Map<Blog>(model);

            return Task.Run(() => Database.BlogRepository.AddAsync(blog));
        }

        public Task UpdateAsync(BlogModel model)
        {
            

            var blog = _mapper.Map<Blog>(model);

            return Task.Run(() => { Database.BlogRepository.Update(blog); Database.SaveAsync(); });

        }

        public Task DeleteByIdAsync(int modelId)
        {
            return Task.Run(() => { Database.BlogRepository.DeleteByIdAsync(modelId); });
        }
    }
}
