﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }
        readonly IMapper _mapper;

        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            _mapper = mapper;
            Database = uow;
        }

        public IEnumerable<UserModel> GetAllWithDetails()
        {
            var users = Database.UserRepository.FindAllWithDetails();

            return _mapper.Map<IEnumerable<UserModel>>(users);
        }

        public IEnumerable<BlogModel> GetBlogsByUserId(int id)
        {
            var blogs = Database.UserRepository.GetByIdWithDetailsAsync(id).Result.UserBlogs;

            return _mapper.Map<IEnumerable<BlogModel>>(blogs);
        }

        public IEnumerable<UserModel> GetAll()
        {
            var users = Database.UserRepository.FindAll();

            return _mapper.Map<IEnumerable<UserModel>>(users);
        }

        public async Task<UserModel> GetByIdAsync(int id)
        {
            var user = await Database.UserRepository.GetByIdAsync(id);
            var role = await Database.UserRepository.GetUserRoleByIdAsync(id);
            var userModel = _mapper.Map<UserModel>(user);
            userModel.Role = role;
            return _mapper.Map<UserModel>(userModel);
        }

        

        public Task AddAsync(UserModel model)
        {
            var user = _mapper.Map<User>(model);

            return Task.Run(() => Database.UserRepository.AddAsync(user));
        }

        public Task UpdateAsync(UserModel model)
        {
            var user = _mapper.Map<User>(model);

            return Task.Run(() => { Database.UserRepository.Update(user); Database.SaveAsync(); });
        }

        public Task DeleteByIdAsync(int userId)
        {
            return Task.Run(() => { Database.UserRepository.DeleteByIdAsync(userId); 
               // Database.SaveAsync(); 
            });
        }

        public Task BanUser(int userId)
        {
            return Task.Run(() => Database.UserRepository.BanUser(userId));
        }
    }
}
