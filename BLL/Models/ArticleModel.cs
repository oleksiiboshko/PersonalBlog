﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DAL.Entities;

namespace BLL.Models
{
    public class ArticleModel
    {
        public int ArticleId { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public DateTime Created { get; set; }

        public int BlogId { get; set; }

        public BlogModel Blog { get; set; }
        
        public ICollection<ArticleTagModel> Tags { get; set; }
        
        public ICollection<ReviewModel> ArticleReviews { get; set; }
    }
}
