﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class TagModel
    {
        public int TagId { get; set; }

        public string Name { get; set; }

        public bool isEnabled { get; set; }

        public ICollection<ArticleTagModel> Articles { get; set; }
    }
}
