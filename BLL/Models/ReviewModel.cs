﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ReviewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public DateTime Created { get; set; }

        public int UserId { get; set; }

        public int ArticleId { get; set; }

        public ArticleModel Article { get; set; }

        public UserModel User { get; set; }

    }
}
