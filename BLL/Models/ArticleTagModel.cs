﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ArticleTagModel
    {
        public int ArticleId { get; set; }

        public ArticleModel Article { get; set; }

        public int TagId { get; set; }

        public TagModel Tag { get; set; }
    }
}
