﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Text;
using DAL.Entities;

namespace BLL.Models
{
    public class BlogModel
    {

        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime Created { get; set; }

        public bool isEnabled { get; set; }

        public int UserId { get; set; }

        public UserModel User { get; set; }

        public ICollection<ArticleModel> BlogArticles { get; set; }

    }
}
