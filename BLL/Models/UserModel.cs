﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public bool isEnabled { get; set; }

        public string PasswordHash { get; set; }

        public string Role { get; set; }

        public ICollection<BlogModel> Blogs { get; set; }
    }
}
