﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BLL.Models;
using DAL.Entities;

namespace BLL.Automapper
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Blog, BlogModel>().ReverseMap();

            CreateMap<Article, ArticleModel>().ReverseMap();

            CreateMap<Review, ReviewModel>().ReverseMap();

            CreateMap<User, UserModel>().ReverseMap();

            CreateMap<Role, RoleModel>().ReverseMap();

            CreateMap<Tag, TagModel>().ReverseMap();

            CreateMap<ArticleTag, ArticleTagModel>().ReverseMap();
        }
    }

}
